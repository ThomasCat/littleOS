/*
littleOS
    Copyright (C) 2020  Owais Shaikh

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
String SECURITY_PINCODE="1234"; //Please change this. Set to 0 to disable

#define BAUDRATE 19200
#include <EEPROM.h>

class kernelComponents{
public:
  int i=0;
  unsigned long timerValue;
  boolean loginSuccess=false;
  void ROMControl(String mode, String data){
    if(mode=="u"){
      while(i<EEPROM.length()){
        int r=EEPROM.read(i);
        if(r==0){
          break;
        }
        i++;
      }
      Serial.print(i);
      i=0;
    }else if(mode=="l"){
      while(i<EEPROM.length()){
        int r=EEPROM.read(i);
        if(r==0){
          break;
        }
        i++;
      }
      Serial.print(EEPROM.length()-i);
      i=0;
    }else if(mode=="reset"){
      while(i<EEPROM.length()){
        EEPROM.write(i, 0);
        i++;
      }
      i=0;
      Serial.print("\n * "); delay(600);
      Serial.print("* "); delay(600);
      Serial.print("* "); delay(600);
      Serial.println("\nEEPROM ERASED [ ✓ ]");    
    }else if(mode=="w"){
      while(i<EEPROM.length()){
        EEPROM.write(i, data[i]);
        i++;
      }
      i=0;
    }else if(mode=="filenames"){
      while(i<EEPROM.length()){
        byte z=EEPROM.read(i);
        char a=char(z); // Converts the byte value back to a character
        Serial.print(a); // This will show the character value as per ascii table
        if(EEPROM.read(i)=='\n'){
          break;
        }else if(EEPROM.read(i)==0){
          Serial.println("Directory empty!");
          break;
        }
        i++;
      }
      i=0;
    }
  }
  long timer(String scale){
    timerValue = millis();
    if(scale=="ms"){
      return timerValue;
    }else if(scale=="s"){
      timerValue = timerValue/1000;
      return timerValue;
    }else if(scale=="m"){
      timerValue = (timerValue/1000)/60;
      return timerValue;
    }
  }
  void textEditor(){
    Serial.print("\nEnter filename: ");
    Serial.flush(); //flush all previous received and transmitted data
    while(!Serial.available()) ; // hang program until a byte is received notice the ; after the while()
    String fileBuffer=Serial.readString();//get filename
    Serial.print(fileBuffer);
    if(fileBuffer.indexOf(".")==-1||fileBuffer.indexOf("")>0){
      Serial.println("Invalid file name. Must have an extension.");
    }else{
      Serial.println("_________________________________\n\'~\' - terminate, \';\' - line ending\n++++++++++++++\n\n");
      Serial.flush(); //flush all previous received and transmitted data
      while(!Serial.available()) ; // hang program until a byte is received notice the ; after the while()
      fileBuffer.concat(Serial.readString());
      if(fileBuffer.indexOf("~")>0){
        Serial.println(fileBuffer);
        ROMControl("w" ,fileBuffer);
        Serial.println("******\nWrote file! [ ✓ ]");
      }else{
        Serial.println("Error: Not saved. Please terminate by adding \'~\' at the end of text.");
      }
    }
  }
  void runScript(){
    bool fileExists=true;
    while(i<EEPROM.length()){
      byte z=EEPROM.read(i);
      char a=char(z); // Converts the byte value back to a character
      if(EEPROM.read(i-1)=='s'){
        if(EEPROM.read(i-2)=='.'){
          Serial.println("Script file found!");
          fileExists=true;
          break;
        }
      }else{
        fileExists=false;
      }
      i++;
    }
    if(fileExists==true){
      while(i<EEPROM.length()){
        if(EEPROM.read(i+2)=='o'&&EEPROM.read(i+3)=='n'){
          if(EEPROM.read(i+5)=='2'){
            pinMode(2, OUTPUT);
            digitalWrite(2, HIGH);
          }else if(EEPROM.read(i+5)=='3'){
            pinMode(3, OUTPUT);
            digitalWrite(3, HIGH);
          }else if(EEPROM.read(i+5)=='4'){
            pinMode(4, OUTPUT);
            digitalWrite(4, HIGH);
          }else if(EEPROM.read(i+5)=='5'){
            pinMode(5, OUTPUT);
            digitalWrite(5, HIGH);
          }else if(EEPROM.read(i+5)=='6'){
            pinMode(6, OUTPUT);
            digitalWrite(6, HIGH);
          }else if(EEPROM.read(i+5)=='7'){
            pinMode(7, OUTPUT);
            digitalWrite(7, HIGH);
          }else if(EEPROM.read(i+5)=='8'){
            pinMode(8, OUTPUT);
            digitalWrite(8, HIGH);
          }else if(EEPROM.read(i+5)=='9'){
            pinMode(9, OUTPUT);
            digitalWrite(9, HIGH);
          }else if(EEPROM.read(i+5)=='1'&&EEPROM.read(i+6)=='0'){
            pinMode(10, OUTPUT);
            digitalWrite(10, HIGH);
          }else if(EEPROM.read(i+5)=='1'&&EEPROM.read(i+6)=='1'){
            pinMode(11, OUTPUT);
            digitalWrite(11, HIGH);
          }else if(EEPROM.read(i+5)=='1'&&EEPROM.read(i+6)=='2'){
            pinMode(12, OUTPUT);
            digitalWrite(12, HIGH);
          }else if(EEPROM.read(i+5)=='1'&&EEPROM.read(i+6)=='3'){
            pinMode(13, OUTPUT);
            digitalWrite(13, HIGH);
          }
        }else if(EEPROM.read(i+2)=='o'&&EEPROM.read(i+3)=='f'&&EEPROM.read(i+4)=='f'){
          if(EEPROM.read(i+5)=='2'){
            pinMode(2, OUTPUT);
            digitalWrite(2, LOW);
          }else if(EEPROM.read(i+6)=='3'){
            pinMode(3, OUTPUT);
            digitalWrite(3, LOW);
          }else if(EEPROM.read(i+6)=='4'){
            pinMode(4, OUTPUT);
            digitalWrite(4, LOW);
          }else if(EEPROM.read(i+6)=='5'){
            pinMode(5, OUTPUT);
            digitalWrite(5, LOW);
          }else if(EEPROM.read(i+6)=='6'){
            pinMode(6, OUTPUT);
            digitalWrite(6, LOW);
          }else if(EEPROM.read(i+6)=='7'){
            pinMode(7, OUTPUT);
            digitalWrite(7, LOW);
          }else if(EEPROM.read(i+6)=='8'){
            pinMode(8, OUTPUT);
            digitalWrite(8, LOW);
          }else if(EEPROM.read(i+6)=='9'){
            pinMode(9, OUTPUT);
            digitalWrite(9, LOW);
          }else if(EEPROM.read(i+6)=='1'&&EEPROM.read(i+7)=='0'){
            pinMode(10, OUTPUT);
            digitalWrite(10, LOW);
          }else if(EEPROM.read(i+6)=='1'&&EEPROM.read(i+7)=='1'){
            pinMode(11, OUTPUT);
            digitalWrite(11, LOW);
          }else if(EEPROM.read(i+6)=='1'&&EEPROM.read(i+7)=='2'){
            pinMode(12, OUTPUT);
            digitalWrite(12, LOW);
          }else if(EEPROM.read(i+6)=='1'&&EEPROM.read(i+7)=='3'){
            pinMode(13, OUTPUT);
            digitalWrite(13, LOW);
          }
        }
        i++;
      }
    }else{
      Serial.println("No script file found!");
    }
    i=0;
  }
  void authenticationUI(){
    loginSuccess=false;
    if(SECURITY_PINCODE=="0"){
      loginSuccess=true;
    }else{
      Serial.print("\nEnter PIN: ");
      Serial.flush(); //flush all previous received and transmitted data
      while(!Serial.available()) ; // hang program until a byte is received notice the ; after the while()
      String p=Serial.readString();
      p.trim();
      Serial.println(p);
      if(p==SECURITY_PINCODE){
        loginSuccess=true;
        Serial.println("\nAuthenticated [ ✓ ]");
      }else{
        Serial.println("Wrong PIN! [ x ]");
        authenticationUI();
      }
    }
  }
  void shell(){
    while(loginSuccess==true){
      Serial.print("\n>> ");
      Serial.flush();
      while(!Serial.available()) ;
      String enteredCommand=Serial.readString();
      Serial.print(enteredCommand);
      Serial.print("\n");
      enteredCommand.trim();
      if(enteredCommand=="help"){
        Serial.println("*  LITTLEOS OPERATION MANUAL *");
        Serial.println("LittleOS (c) 2020 Owais Shaikh\nI wrote this OS for my Operating Systems class. It supports 10 commands:\n");
        Serial.println("help - display this manual");
        Serial.println("list - list all files");
        Serial.println("edit - create or edit file");
        Serial.println("run - execute script");
        Serial.println("stats -  show statistics");
        Serial.println("reset - wipe ROM");
      }else if(enteredCommand=="reset"){
        Serial.println("Cannot be undone and reduces EEPROM lifespan. Are you sure? (y/n)");
        while(!Serial.available()) ;
        Serial.flush();
        String choice=Serial.readString();
        choice.trim();
        if(choice=="y"){
          authenticationUI();
          if(loginSuccess==true){
            ROMControl("reset", "");
          }else{
            Serial.println("EEPROM not erased! [ x ]");
          }
        }else{
          Serial.println("EEPROM not erased! [ x ]");
        }
      }else if(enteredCommand=="stats"){
        Serial.print("Uptime: ");
        if(timer("s")<60){
          Serial.print(timer("s"));
          Serial.print(" seconds\n");
        }else{
          Serial.print(timer("m"));
          if(timer("m")<=1){
            Serial.print(" minute\n");
          }else{
            Serial.print(" minutes\n");
          }
        }
        Serial.print("Storage used: ");
        ROMControl("u", "");
        Serial.print(" bytes");
        Serial.print("\nStorage left: ");
        ROMControl("l", "");
        Serial.print(" bytes");
      }else if(enteredCommand=="edit"){
        textEditor();
      }else if(enteredCommand=="list"){
        ROMControl("filenames", "");
      }else if (enteredCommand=="run"){
        runScript();
      }else{
        Serial.println("Not a recognized command. Type 'help' for a list of commands.");
      }
    }
  }
}; kernelComponents kernel;

class bootscreenService{
public:
  void bootVerbose(){
    delay(500);
    Serial.println("Output device...      [ ✓ ]");
    delay(500);
    Serial.println("Starting timer...     [ ✓ ]");
    delay(500);
    Serial.println("Reading EEPROM...     [ ✓ ]");
    delay(500);
    Serial.println("Loading shell...      [ ✓ ]");
    delay(500);
    Serial.println("Starting lockscreen...[ ✓ ]");
    delay(500);
  }
  void bootSplash(){
    Serial.println("                             █████   ████\n                            █     █ █    █\n█    █  █    █   █     ███  █     █ █\n█      ███  ███  █    █   █ █     █  ████\n█    █  █    █   █    ████  █     █      █\n█    █  █    █   █    █     █     █ █    █\n████ █  ███  ███  ███  ███   █████   ████");                                                 
  }
  void boot(){
    Serial.begin(BAUDRATE);
    kernel.timer("ms");
    bootVerbose();
    bootSplash();
    kernel.authenticationUI();
  }
}; bootscreenService bootscreen;

void setup(){
  bootscreen.boot();
}

void loop(){
  kernel.shell();
}