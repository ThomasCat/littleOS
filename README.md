# littleOS

littleOS is a small RTOS that can store data to the EEPROM, display it and run tiny scripts. I made it as an assignment for my Operating Systems class. 

### Instructions
1. Upload program, connect Arduino to computer and open Serial Monitor (Ctrl+Shift+M).
2. Enter your passcode (the default is <i>1234</i>, this can be changed in the source code).
3. Type ```help``` to get a list of commands.

#### To run a simple script:
1. Open a file by typing ```edit``` in the terminal.
2. Name the file whatever you'd like, with a ```.s``` extension, for example: ```test.s```.
3. To turn an on digital pin <i>D13</i>, type:

```
on 13~
```

4. Save the file by pressing Enter.
5. Type ```run``` to run the script.

[Open-source notices](NOTICE)

<b>License</b>: 

<a href="http://www.gnu.org/licenses/gpl-3.0.en.html" rel="nofollow"><img src="https://camo.githubusercontent.com/0e71b2b50532b8f93538000b46c70a78007d0117/68747470733a2f2f7777772e676e752e6f72672f67726170686963732f67706c76332d3132377835312e706e67" alt="GNU GPLv3 Image" data-canonical-src="https://www.gnu.org/graphics/gplv3-127x51.png" width="80"></a><br>[Copyright © 2020  Owais Shaikh](LICENSE)